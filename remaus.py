# Remaus
# Remove Emacs Autosaves
# Transitioning from Emacs to Sublime Text
# This script removes all Emacs autosave files
# *~
# *.*~ 
# #*#
# #*.*# 
# *#
# *.*#

from subprocess import call
import os
import sys
import getopt

def print_help():
  print("Usage: python3 remaus.py [dir]")
  print("-r\t\tRecursively delete save files in every subdirectory")
  print("--help\t\tPrints this help.")

def remove(dir, recursive=False):
  for (dirpath, dirnames, filenames) in os.walk(dir):
    for filename in filenames:
      if filename[-1] == '~' or filename[-1] == '#':
        call(['rm', dirpath + '/' + filename])
    if not recursive:
      return

if __name__ == '__main__':
  try:
    recursive = False
    (opt, args) = getopt.getopt(sys.argv[1:], 'r', ['help'])
    for (param, arg) in opt:
      if param == '-r':
        recursive = True
      elif param == '--help':
        print_help()
        sys.exit(2)
    remove(args[0], recursive)
  except getopt.GetoptError as err:
    print(str(err))
    print_help()
    sys.exit(2)
